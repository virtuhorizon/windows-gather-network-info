#Requires -version 3.0




#generate ip list from csv
# loop through ips
$input_list="iplist.csv"
$iplist_csv = Import-Csv -Delimiter "`t" -Path $input_list

$folder = "report-$(get-date -f yyyy-MM-dd)"
New-Item -ItemType directory -Path $folder


$iplist = @()
foreach($client in $iplist_csv){
    $ip = $client.IP
    $status = $client.Status
    #check if client was detected as alive from iplist
    if($status -eq "Ok"){ 
      $iplist += $ip
    }
}





#main loop
$ScriptBlock = {
   Param ( [string]$ip,[string]$folder )

   & powershell.exe -file "node.ps1" -ip "$ip" -folder "$folder" -drive_root "C"
   Return $ip

}



# Async process
# 25 is the max to avoid crashes
$MaxThreads = 5
$runspacePool = [RunspaceFactory ]::CreateRunspacePool(1, $MaxThreads)
$runspacePool.Open()


$pipeLines = foreach($ip in $iplist){

$pipeline = [powershell]::Create()
$pipeline.RunspacePool = $runspacePool
$pipeline.AddScript($ScriptBlock)    | Out-Null   
$pipeline.AddArgument($ip)  | Out-Null
$pipeline.AddArgument($folder)  | Out-Null
$pipeline | Add-Member -MemberType NoteProperty -Name 'AsyncResult' -Value $pipeline.BeginInvoke() -PassThru 
}



#obtain results as they come.
$results =  foreach($pipeline in $pipeLines){
$pipeline.EndInvoke($pipeline.AsyncResult )
}


 #cleanup code.
 $pipeLines | % { $_.Dispose()}
 $pipeLines = $null
 if ( $runspacePool ) { $runspacePool.Close()}


# Merge files

$getFirstLine = $true

get-childItem "$folder\*.csv" | foreach {
    $filePath = $_

    $lines =  $lines = Get-Content $filePath  
    $linesToWrite = switch($getFirstLine) {
           $true  {$lines}
           $false {$lines | Select -Skip 1}

    }

    $getFirstLine = $false
    Add-Content "merged-$folder.csv" $linesToWrite
    }

 #your results
 $results

