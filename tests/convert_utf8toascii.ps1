#convert utf8 to ascii inplace

$filename = "iplist-utf8.csv"
$filename2 = "iplist.csv"
[io.file]::ReadAllText($filename, [System.Text.Encoding]::utf8) | %{[io.file]::WriteAllText($filename2, $_, [System.Text.Encoding]::ASCII)}
