#generate ip list from csv
# loop through ips
$iplist_csv = Import-Csv "iplist.csv"


$iplist = @()
foreach($client in $iplist_csv){
    $ip = $client.IP
    $status = $client.Status
    #check if client was detected as alive from iplist
    if($status -eq "Ok"){ 
      $iplist += $ip
      Write-Host $ip
    }
}
Write-Host $iplist