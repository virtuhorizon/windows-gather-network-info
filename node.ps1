param([string]$ip='127.0.0.1',[string]$folder="report-localhost",[string]$drive_root="C") #Must be the first statement in your script


#$env:computername
#$env:programfiles
#$env:programfiles(x86)
#$env:computername






$arr_files = @()
#create folder if it doesnt exist
New-Item -ItemType Directory -Force -Path $folder

$start_time = Get-Date

Write-Host "Started @: $start_time"

try {

	# Write-Host $ip

	$Output_name = $ip + "-"+ $drive_root+"-report.csv"
	$root = '\\' + $ip +"\" +$drive_root + "`$\"
	

	# Write-Host $root



	# Get list of folders on remote C$
	$arr_dirs = Get-ChildItem $root | 
       Where-Object {$_.PSIsContainer} | 
       Foreach-Object {$_.Name}

    # change array to arraylist for easy key removal from collection type
    $arrlist_dirs = New-Object System.Collections.ArrayList(,$arr_dirs)

    # pop keys
	$arrlist_dirs.Remove("Windows")
	$arrlist_dirs.Remove("inethub")
	# $arrlist_dirs.Remove("DFSRoots")
	# $arrlist_dirs.Remove("Users")

	
	
	foreach($cpath in $arrlist_dirs){
	$check_path = $root + $cpath

	Write-Host "Checking: " $check_path
	try{

		gci $check_path -recurse | ? {$_.PSIsContainer -eq $False -and $_.Extension -eq ".exe"} | % {
  			$obj = New-Object PSObject
  			

			$splitSearch = '$drive_root`$'
			$filtered  = $_.DirectoryName.Split($splitSearch,3)
			$local_path =$drive_root+":" + $filtered[2] 			
  			
  			


  			$obj | Add-Member NoteProperty Directory $_.DirectoryName
  			$obj | Add-Member NoteProperty LocalDirectory $local_path
  			$obj | Add-Member NoteProperty Name $_.Name
  			$obj | Add-Member NoteProperty Extension $_.Extension
  			$obj | Add-Member NoteProperty Length $_.Length
  			$obj | Add-Member NoteProperty Owner ((Get-ACL $_.FullName).Owner)
  			$obj | Add-Member NoteProperty Computer ((Get-ACL $_.FullName).Computer)
  			$arr_files += $obj
  		}
  	} catch{
  		Write-Host $Error[0].Exception
  	}
  
	}
    




}finally{
	# write progress to csv
	$arr_files | Export-CSV -notypeinformation $folder\$Output_name
	Write-Host "Wrote: $Output_name"

	$end_time = Get-Date

	Write-Host "Started @: $start_time"
	Write-Host "Ended   @: $end_time"
}
