# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1
This powershell script is designed to take an input list of valid IP addresses on the network, open each C$ directory and recursively scan everyfolder in c ( not including:

 - c:\windows
 - c:\DFSRoots
 - c:\inethub
 - c:\Users

* each IP address will have a csv created in a subfolder where this script was executed with a timestamp.

* At the end, once all ip addresses have been scanned or have failed, each csv file will be merged into one report in the same directory as the script.

* This script runs semi-asyncronously, with a 25 concurrent node limit to avoid crashing. this may be increased.



### How do I get set up? ###

* ~~DOWNLOAD the latest version of the repo here ( zip format ) https://bitbucket.org/virtuhorizon/windows-gather-network-info/downloads~~
* ~~unzip~~
* Download git portable for windows:
* https://github.com/git-for-windows/git/releases/download/v2.10.1.windows.1/PortableGit-2.10.1-32-bit.7z.exe
* run provided git clone/ pull script; Or:
*
```
#!bash

git clone https://virtuhorizon@bitbucket.org/virtuhorizon/windows-gather-network-info.git
```

* Dependencies: you will need to supply a list of IP addresses to check. Example in "iplist.csv"
* Database configuration: Just make sure the path that the script is executed in is writeable
* Deployment instructions:

Open cmd as domain administrator




> powershell .\init-async.ps1



### Issues ###

* Powershell execution policies

* First thing to do is to unblock both powershell script files from NTFS Alternate File Steams:
* ![unblock-scripts.PNG](https://bitbucket.org/repo/kE55p5/images/2996379365-unblock-scripts.PNG)

* next open powershell

```
#!powershell
Get-ExecutionPolicy
Set-ExecutionPolicy Unrestricted
```


* remember to reset when finished to whatever it was before, such as:
```
#!powershell
Set-ExecutionPolicy Restricted
```






* With threads set to 25, cpu usage can skyrocket for a few minutes, or depending on how large of a scan it is

* ![cpu usage.PNG](https://bitbucket.org/repo/kE55p5/images/3899135526-cpu%20usage.PNG)